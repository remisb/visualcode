package lt.bauzys.visual.client;

import net.edzard.kinetic.Box2d;
import net.edzard.kinetic.Circle;
import net.edzard.kinetic.Kinetic;
import net.edzard.kinetic.Layer;
import net.edzard.kinetic.Rectangle;
import net.edzard.kinetic.Stage;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.RootPanel;

 /**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class VisualCode implements EntryPoint {

  public void onModuleLoad() {
	    // Kinetic needs a special div in the DOM
	    Element div = DOM.createDiv();
	    RootPanel.getBodyElement().appendChild(div);

	    // Setup stage
	    Stage stage = Kinetic.createStage(div, 400, 400);
	    Layer layer = Kinetic.createLayer();
	    stage.add(layer);

	    Rectangle c = Kinetic.createRectangle(new Box2d(10, 10, 200, 200)); 
	    layer.add(c);
	    Circle c2 = Kinetic.createCircle(new Vector2d(100, 100), 10);
	    layer.add(c2);
	    stage.draw();
  }
}
